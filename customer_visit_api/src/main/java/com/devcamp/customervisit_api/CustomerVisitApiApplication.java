package com.devcamp.customervisit_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerVisitApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerVisitApiApplication.class, args);
	}

}
